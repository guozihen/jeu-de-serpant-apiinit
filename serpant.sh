declare -i tputlines; #height de console -i = integer -a = list ou array
declare -i tputcols;  #weight de console
declare -i HIGHT
declare -i WIDTH
declare -ia map;
declare -a way; #direction actuel，direction de traget
declare -a xsnackBody; #defini 0 comme le head
declare -a ysnackBody;
declare -i l_snack;
declare -i PIDSnack;
declare -ia snackfood;
declare gamestaus;
declare sl=0.5

#fonction initMap pour initialiser les valeurs qu'on a besoin
function initMap() # variable，x,y
{
    tputlines=`tput lines`; #taille de ligne
    tputcols=`tput cols`; #taille de colonne
    HIGHT=$((${tputlines}-3)); #taille de ligne de jeu
    WIDTH=$((${tputcols}-50)); #taille de colonne de jeu
    map[0]=1;        #x0
    map[1]=1;        #y0
    map[2]=${WIDTH}; #x1
    map[3]=${HIGHT}; #y1
}

#En utilisant les variable qu‘on a obtient par initMap, le methode for peut dessiner simplement le map de notre jeu
function printMap()
{
    for ((i=0; i<=$WIDTH; i++))
    {
        for ((j=0; j<=$HIGHT; j++))
        {
            if [[ $i == ${map[0]} || $i == ${map[2]} ]] || [[ $j == ${map["1"]} || $j == ${map["3"]} ]];then
            {
                print "wall" $i $j; #print la fonction on a defini c'est pas printf
            };fi;
        }
    }
}

#fonction print pour dessiner, par exemple si on a print"wall" c-a-d utiliser le cas "wall"
#\e... c'est juste les formules dans linux pour mettre en couleur(text et plan arriere)
#case est comme dans autre langages, ;; represent break et esac pour finir
function print()
{
    case "${1}" in
        #"map")  echo -e "\e[${3};${2}H\e[44m \e[0m";; #echo -e pour eviter \
        "wall") echo -e "\e[${3};${2}H\e[42;30m#\e[0m";;
        "head") echo -e "\e[${3};${2}H\e[42;30m*\e[0m";;
        "body") echo -e "\e[${3};${2}H\e[42m+\e[0m";;
        "tail") echo -e "\e[${3};${2}H\e[42;30m-\e[0m";;
        "food") echo -e "\e[${3};${2}H\e[42;30m@\e[0m";;
        "clear") echo -e "\e[${3};${2}H \e[0m";; #\e[40m
    esac;
}

function clearTerminal()
{
    echo -ne "\e[2J"; #effacer le tableau laisser 2 ligne d'espace on peut faire \e[0] ou \e[1] aussi
}

#cycle de vie de local : dans la fonction
#let pour effectuer le calcule
function initSnack()
{
    local ymid;
    let ymid=\(${map[1]}+${map[3]}\)\/2 # centre de notre espace
    local xmid;
    let xmid=\(${map[0]}+${map[2]}\)\/2 # centre de notre espace
    way[0]="right";  #direction a droite initialement
    way[1]="right";
    l_snack=$1;

    for((i=0;i<$l_snack;i++))
    {
        let xsnackBody[$i]=$xmid-$i;
        let ysnackBody[$i]=$ymid;
    }
}

#Juste utiliser la fonction print, comme on a fait pour dessiner le map
function printSnack()
{
    print "head" ${xsnackBody[0]} ${ysnackBody[0]};
    for((i=1;i<l_snack-1;i++))
    {
        print "body" ${xsnackBody[$i]} ${ysnackBody[$i]};
    }
    print "tail" ${xsnackBody[((${l_snack}-1))]} ${ysnackBody[((${l_snack}-1))]};
}

#la movement dans un direction qu'on a besoin
function movetoNext()
{
    snackEat
    print "clear" ${xsnackBody[((${l_snack}-1))]} ${ysnackBody[((${l_snack}-1))]}
    for((i=$l_snack;i>0;i--))
    {
        ysnackBody[$i]=${ysnackBody[((${i}-1))]};
        xsnackBody[$i]=${xsnackBody[((${i}-1))]};
    }
    case ${way[0]} in
    "up")   let ysnackBody[0]-- ;;
    "down")   let ysnackBody[0]++ ;;
    "left")   let xsnackBody[0]-- ;;
    "right")   let xsnackBody[0]++ ;;
    esac;
    gameOverTest;
    printSnack;
}

#changer la direction
#on peut pas faire up puis down ou left puis right directement.
function turnway() #up,down,left,right
{
    if [[ ${way[0]} != ${way[1]} ]];then
        if ! (([ ${way[0]} == "up" -a ${way[1]} == "down" ] || [ ${way[0]} == "down" -a ${way[1]} == "up" ]) || \
            ([ ${way[0]} == "left" -a ${way[1]} == "right" ] || [ ${way[0]} == "right" -a ${way[1]} == "left" ]));then
            way[0]=${way[1]};
        fi
    else
        movetoNext;
    fi
}

#on controler la direction
function readisign()
{
    case $1 in
    "A") way[1]="up" ;;
    "B") way[1]="down" ;;
    "C") way[1]="left" ;;
    "D") way[1]="right" ;;
    "g") sl=$(speedup $sl) ;;
    "h") sl=$(speeddown $sl) ;;
    esac;
    turnway;
}

function createFood()
{
    snackfood[0]=$(($RANDOM%($WIDTH-2)+2))
    snackfood[1]=$(($RANDOM%($HIGHT-2)+2))
    print "food" ${snackfood[0]} ${snackfood[1]}
}

#on compare le head de snake et snackfood si c'est la meme position donc on ajoute 1 dans la longeur de snake et on creer un nouveau food
function snackEat()
{
    if [ ${xsnackBody[0]} == ${snackfood[0]} -a ${ysnackBody[0]} == ${snackfood[1]} ];then
        let l_snack++;
        createFood;
    fi;
}

function gameoverinfo()
{
    gamestaus="over"
#    kill -30 $PPID
    echo "           Game Over!!!      ";
    echo "       Ctrl+C pour quitter      ";
}

#les condition perdu
function gameOverTest()
{
    local gameover="false";
    for((i=1;i<l_snack;i++))
    {   # snake se touche
        if [ ${xsnackBody[0]} == ${xsnackBody[$i]} ] && [ ${ysnackBody[0]} == ${ysnackBody[$i]} ];then
            gameover="true";
        fi;
    }
    #snake touche le wall
    if [ ${xsnackBody[0]} == ${map[0]} ] || [ ${xsnackBody[0]} == ${map[2]} ] \
    || [ ${ysnackBody[0]} == ${map[1]} ] || [ ${ysnackBody[0]} == ${map[3]} ];then
        gameover="true"
    fi;
    #on affiche les informations gameover
    if [ $gameover == "true" ];then
        gameoverinfo;
    fi;
}


function initSnackTrap()
{   #on recu le signal envoye par readinput et on rappelle la fonction readisign
    trap "readisign A" 35
    trap "readisign B" 36
    trap "readisign D" 37
    trap "readisign C" 38
    trap "readisign g" 39
    trap "readisign h" 40
    trap "exit 2" 2

}

function speedup()
{
    echo $1 + 0.1 | bc
}

function speeddown()
{
    if (( $(echo "$1 > 0.05" | bc -l) )) ; then
        echo $1 - 0.05 | bc
    else
        echo $1
    fi
}


function snackProcess()
{
    #local v
    #$v = \(${vitesse}\)\/2
    tput civis;
    initSnackTrap
    initMap
    printMap
    createFood
    initSnack 6
    movetoNext;
    gamestaus="normal"
    while [ ${gamestaus} == "normal" ];do
    {
        movetoNext;
        sleep $sl;
    };done;
}

#up - "\033[A"
#down - "\033[B"
#left - "\033[D"
#right - "\033[C"
# read -s(Silence donc il affiche pas sur terminal quand on lit input)t(lire input chaque n second) 1(1 second)  -n 1 input(le 1ere entree est stocke dans input  )
function readinput()
{
    local input;
    while(true);do
    {
        read -st 1 -n 1 input;

        if [[ $input == $'\033' ]];then
            read -st 1 -n 1 input;
            if [[ $input == '[' ]];then
                read -st 1 -n 1 input;
                case $input in
                "A") `kill -35 $PIDSnack`;;#envoyer 35 a pidsnack
                "B") `kill -36 $PIDSnack`;;
                "D") `kill -38 $PIDSnack`;;
                "C") `kill -37 $PIDSnack`;;
                esac;
            fi;
        fi;
        if [[ $input == 'g' || $input == 'h' ]];then
            case $input in
            'g') `kill -39 $PIDSnack`;;
            'h') `kill -40 $PIDSnack`;;
            esac;
        fi;
    };done;
}

function exitGame()
{
    kill -2 $PIDSnack
    tput cnorm; #afficher le curseur
    recoverTerminal;
    exit 2;
}

function initReadTrap()
{
    trap "exitGame" 2
}

function initTerminal()
{
    clearTerminal;
    initReadTrap;
    readinput;
}

function recoverTerminal()
{
    tput cup $tputlines 0
    clearTerminal
}

function readProcess()
{
    initTerminal;
}

function help()
{
    echo "
                    /^\/^\\
                  _|__|  O|
         \/     /~     \_/ \\
          \____|__________/  \\
                 \\_______      \\
                          \     \                 \\
                           |     |                  \\
                          /      /                    \\
                         /     /                       \\
                       /      /                         \\ \\
                      /     /                            \\  \\
                    /     /             _----_            \\   \\
                   /     /           _-~      ~-_         |   |
                  (      (        _-~    _--_    ~-_     _/   |
                   \      ~-____-~    _-~    ~-_    ~-_-~    /
                     ~-_           _-~          ~-_       _-~
                        ~--______-~                ~-___-~"
    echo -e "\n\n\n"
    echo -e "                       Bienvenu ！\n"
    echo -e "         Snake simple"
    echo -e "         up down left right pour controler ! Bonne chance\n"
    for ((i=2;i>0;i--))
    {
        echo "${i}s commence le jeu";
        sleep 1;
    }
}

if [ "$1" = "-h" ]
#printf  ou echo -n pour ne pas passer à la ligne
then
echo "Voici le guide du jeu : 

Pour se déplacer :       ↑
                      ←  ↓ → 


aller vers le haut=↑
aller vers à gauche=←     aller vers la droite=→
aller vers le bas=↓
vitesse up : h
vitesse down : g

Pour quitter : Ctrl+C
 "
else

help
#Mettre la commande en arrière-plan pour l'exécution
snackProcess &
#Le PID du processus d'arrière-plan que Shell a exécuté pour la dernière fois
PIDSnack=$!
readProcess;
fi
