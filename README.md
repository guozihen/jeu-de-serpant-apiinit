Jeu serpant

1. Lecture clavier et multi-process <br/>
    Dans le code, utilisez les touches "haut, bas, gauche, droite" pour contrôler la direction de marche du serpent. Afin de lire la saisie au clavier en temps réel, et le contrôle ne fait pas écho , alors l'état du clavier doit être détecté dans une boucle inifini.

    Afin de rafraîchir la carte normalement en même temps et d'éviter de retarder la lecture du clavier, nous devons introduire plusieurs processus, en plaçant le contrôle et l'affichage du serpent dans un processus séparé ! Ne vous influencez pas, mais communiquez les uns avec les autres !

2. Communication inter-processus et transmission de signaux<br/>
    Le processus clavier lit les données, nous devons utiliser le mécanisme "signal" pour les transmettre au processus de contrôle. L'une des méthodes consiste à utiliser trap pour capturer le signal et appeler la fonction de traitement correspondante en fonction du signal.

3. Contrôle du corps du serpent

4. Affichage de la carte

5. Interaction terminale
